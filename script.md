## a rant

The particulars do not require
the applicant to have worked on a research project,
so it is odd to demand one in the presentation brief.

It makes it look like you're looking to hire someone who is
already a Research Software Engineer.
We already know how white and male and middle class that pool is.
It doesn't look good for your diversity.

## Global Warming Denialists

A few years ago, some people on the internet,
denying the realities of climate change,
attacked NASA's graph showing increasing global temperature.

## Graph

The graph was a lie, the data was manipulated,
the computer program is secret.

NASA then published the, previously unpublished,
source code that creates this graph.
The same wrong people then attacked this code.

The code was unclear, no-one can run it,
this can't be the code that makes the graph.

Two of us decided that we could at least solve the problem that
NASA's code was unclear.

## Taking stock

We took it upon ourselves to take NASA's code,
which was written in a mixture of Fortran and other languages,
and which probably only ever ran on their IBM AIX mainframe,
and rewrite it for clarity.

This would be our small contribution to
the public understanding of science.

## Python

We already knew and liked Python.
Python is clear.
Python has a reputation for being good for beginners.
Python is portable.
Certainly, easier to port than a Fortran program
that was only ever compiled by IBM's Fortran compiler.

## Process

Gilb's Evolutionary Delivery as documented in his 1988 work,
Principles of Software Engineering Management.

(
We consider Agile, XP, and most current Buzzword practices
to be either derived from this, or imitating it.
)
The core of this process is a continual loop of:
- requirements analysis,
  to identify the high-risk and high-value components;
- incremental change;
- evolutionary delivery: Deliver the smallest change with the
  highest value, attack biggest risk first.

In our case the main target was fixed and unlikely to change,
although auxiliary features and requirements did change
(such as what databases to use, what graphs to output in what
formats).

Piecewise replacement made incremental delivery reasonably
straightforward.
Python made incremental improvement reasonably easy.

No formal tests except testing the end-to-end result against the
original.
Integer parts of the algorithm were bitwise identical,
floating point parts "essentially the same", any error more than
1e-6 was suspicious.
Final result identical to < 0.001 Kelvin

## Collaboration

We're a small team and we're Change Management native.

Using public VCS and e-mail was natural to us.

Discussion on a public e-mail list and public issue tracker.

We even, for a short while, attracted a collaborator "from the wild".

Some external collaboration with NASA GISS.
Given their budget, not much time, but they were willing to
provide sanple intermediate data files and complete working
files from a couple of runs which were very useful in our
testing.

Whenever we were reporting bugs with them we always did so in
the spirit of co-operation.

## Success

Replicated software.

Essentially identical result,
much better than could reasonably be expected.
Differences approximately 10 to 100 times smaller than
scientific error estimates;
indistinguishable for published figures.

Found a few bugs (in compilers and the original code).

Did we change the debate?
You no longer see global warming denialists use
obscure source code as a basis for rejecting claims.
Does that matter in the broad?

I did probably about 40% of the "typing in" and about half of
the planning and design and coordination.

Google Summer of Code.
Participated as an organisation in 2 years.
Mentored 5 (post grad) students.

## Challenges

We lost our external collaborator.

It's hard to fix the world pro bono.

To some extent I don't think we ever solved these problems.
The reason the project now languishes is that
we were unable to form a community
and that we never got reliable funding for the Foundation.

FORTRAN. Printouts. Isolating inputs and outputs.
Writing inspection tools
(we wrote a Python program to read and write
Fortran binary record format).

Archaic data formats.
The formats of the various input data were
designed to be used with punched card input. 
Not very Python friendly.
Again, modules to read and write these
(relatively simple, but alien) formats.
