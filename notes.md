# Notes

- Get brief (see below)
- review application (see `README.md`)
- review original job post (see `rse-particulars.md`)
- make plan
- present

# Brief

(from e-mail 2018-08-24)

Please deliver a short presentation (10 mins max)
on a research project you have
previously worked on.
We would like to know:
* The software engineering processes you used in the project
* How you managed collaboration within, and external to, the project
* Key successes, why they were successes, and your contribution to them
* Challenges faced and how you overcame them.
