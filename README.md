# 2018-manchester-job

As part of the Research IT team, you will be responsible for providing software engineering expertise to researchers, research projects and our own platforms while working on engagements from a few weeks to many months in duration.
You will work on research projects from conception to completion, gathering requirements, helping with grant applications, implementing and documenting as required.

We are interested in receiving applications from people who have extensive experience in the complete software engineering lifecycle and who have a passion for supporting researchers.
We are particularly keen to hear from people who have experience in the areas of Data Science, High Performance Computing, Numerical Computation, Web and Mobile Application Development and Sustainable and Reproducible Research.
You will be able to demonstrate proficiency with one or more programming languages such as, but not limited to, Python, R, C, C++, Java, Fortran and Shell.
Experience of computational science would be an advantage.

## drj's personal statement

We must fix the way software is made in The Academy.

Researchers are poorly trained in creating research software, and even less well-trained in making software with a prolonged life and/or a wider user-base.
Expert professionals need to step in and assist, with measures of advice, hands-on design and programming, and training.

I consider myself a programmer of tools and platforms.
I program in C, Python, Go, to make software tools and platforms used by other programmers.
When I do application programming,
it tends to be in circumstances that are more highly constrained (for example, a web-based programming environment).

With reference to the areas highlighted—Data Science, HPC, Numerical Computation, Web and Mobile Application Development, and Sustainable and Reproducible Research—I think that my extensive experience of sofware lifecycle management in a commercial setting gives me a good start in Sustainable and Reproducible Research;
I have a penchant for systems programming and a degree in mathematics so while I have only done a small amount of High Performance Computing and Numerical work,
I would enjoy the opportunity to do more (I know what the "restrict" keyword does in C and I've given talks about floating point at local Python user groups).
Data Science fascinates me, particularly the question of what it is, and what questions can we ask and answer with it. Is it, for example, a map showing Sheffield's tree canopy cover? https://drj11.github.io/sheffield-map/canopy/

I know far too much about Shell and /bin/awk and delight in teaching it to people.

I don't know any R or Java or C++ except really obscure corners,
only useful for hiding in and surprising language nerds.
I would relish the prospect of learning more R because it is widely used
and seems like a gateway through which we can lead people from the badlands of proprietary programming systems into the fertile fields in which Open Source software is grown.

Addressing the Essential Skills and Competencies section of the particulars.

Academic Knowledge: I have a mathematics degree and a postgraduate diploma in computer science.
Giving me a good background in numerical analysis, and the theoretical background to statistics.

Consultancy and Technical Specialism: Time in industry has made me expert in compiler runtime systems, memory management, low-level embedded programming, and microprocessor control.

Application Development Tools: I'm competent in Make, shell, git (and other version control systems such as CVS, SVN, and Perforce; don't make be use SVN though).

Technology Knowledge: I'm not really sure what you want here, but I know Unix pretty deeply. I can make Linux system calls from assembler (they are really badly documented by the way).
While I don't know HPC that well, I know what an AVX instruction is, and it turns out that half of HPC is just knowing /bin/sh quite well and being able to read the qsub(1) man page.

Business Environment: My most recent post in The Academy has, unfortunately, given me some exposure to the grant-making process.
I'm sure RSEs can help with that.

Information Acquisition: Solving customer problems as a consultant at Ravenbrook was largely a matter of understanding the customer's problems in order to identify gaps and devise creative solutions to help them.

Proof of Concept and Prototyping: At Ravenbrook we usually advocated an incremental approach when building solutions.
This would of coourse involve building prototypes.
Ideally prototypes that could then be evolved into increasingly sophisticated solutions.
The best way to help a customer understand their problem is to build the simplest thing that could possibly be useful to them.

Operational/Service Architecture: Obviously I don't know how you folks do it at the University of Manchester, but I do know rouhgly how networks work, and the kinds of operating systems that people might run on them.

Programming Languages: I'm an expert in C, Python, AWK and /bin/sh; I'm competent in Go, Lua; I've had some exposure to Java, 64-bit Intel Assembler.
I enjoy learning new languages. With many of these languages, I have some experience of the surrounding packaing and build tools.

Analytical Thinking: Again, the job of a consultant at Ravenbrook is to creatively break down a customer's problems into manageable pieces where it was both possible to create solutions and to bring the customer to an understanding of the solutions.

IT Environment: I have no idea what you're asking here.

Attention to Detail: I enjoy having the oportunity to work in an environment where I can take pride in my work.

Flexibility: Bespoke solutions to customer problems can fail to take account of the brittleness of the customer's expectations.
They claim to want one thing, but 3 month's later when presented with a solution they abhor it.
Customers change their mind and this should not be seen as a problem.
Consequently, I think systems should generally be flexible in order to accomodate at least some of the "unknown unknowns".

Organisational Awareness: An org chart is one of the most valuable aids when analysing a customer's requirements. Make sure you say the right things to the right people.

Interacting with People: While not the most natural people-person, I do maintain a professional and informal network.
In The Academy I have an appreciation of the value of both formal and informal collaborations and contacts.
I enjoy going to PyCon UK to meet other members of the national Python community, and I also enjoy the odd local meetup.
